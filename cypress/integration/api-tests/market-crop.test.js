/// <reference types="cypress" />

context('Market Crops API tests', () => {

    it('OnoArk - Retrieve all crops', () => {
        cy.request('market-crop/crops')
            .should((response) => {
                expect(response.status).to.eq(200)
                expect(response.body.data).length.greaterThan(0)
                expect(response.body.data[0]).to.have.property('cropId')
            })
    })
    it('OnoArk - Retrieve all markets', () => {
        cy.request('market-crop/markets')
            .should((response) => {
                expect(response.status).to.eq(200)
                expect(response.body.data).length.greaterThan(0)
                expect(response.body.data[0]).to.have.property('marketId')
            })

    })

    it('OnoArk - get Markets With Cities', () => {
        cy.request('price/price-in/city-markets')
            .should((response) => {
                expect(response.status).to.eq(200)
                expect(response.body.data).length.greaterThan(0)
                expect(response.body.data[0]).to.have.property('city')
            })

    })

})
