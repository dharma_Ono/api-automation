/// <reference types="cypress" />
const Generic=require('../../support/Generic')
let today=Generic.getTodayDate()
context('Price In API tests', () => {
    let onoId='180'
    let marketId='6c75a7eb-3df8-4ec5-acf2-0e50068bee12'
    it('OnoArk - Retrieve all crops', () => {
        cy.request('price/price-in/city-markets')
            .should((response) => {
                expect(response.status).to.eq(200)
                expect(response.body.data).length.greaterThan(0)
                expect(response.body.data[0]).to.have.property('markets')
            })
    })
    it('OnoArk - Retrieve all markets', () => {
        cy.request(`price/price-in/date/${today}`)
            .should((response) => {
                expect(response.status).to.eq(200)
                expect(response.body.data).length.greaterThan(0)
                expect(response.body.data[0]).to.have.property('agentName')
            })

    })

    it('OnoArk - Get Daily Price for Prefered crops', () => {
        cy.request(`dashboard/crops/onoId/${onoId}/lat/12.96585106/lon/77.5761149`)
            .should((response) => {
                expect(response.status).to.eq(200)
                // expect(response.body.data).length.greaterThan(0)
                // expect(response.body.data[0]).to.have.property('agentName')
            })

    })

    it('OnoArk - Get today crop prices for market', () => {
        cy.request(`dashboard/crops/onoId/${onoId}/marketId/${marketId}`)
            .should((response) => {
                expect(response.status).to.eq(200)
                // expect(response.body.data).length.greaterThan(0)
                expect(response.body.data).to.have.property('marketId')
            })

    })

})
