/// <reference types="cypress" />

const singInData = require('../../fixtures/signIn.json')

let otpReqId, onoId;
context('Account API tests', () => {

    it('OnoArk - SignUp', () => {
        cy.request('POST', 'account/sign-in', singInData.signIn)
            .should((response) => {
                expect(response.status).to.eq(200)
                expect(response.body.data).to.have.property('onoId')
                expect(response).to.have.property('headers')
                otpReqId = response.body.data.otpReqId
                onoId == response.body.data.onoId
            })
    })
    it('OnoArk - Otp validations', () => {

        singInData.otpValidation.otpReqId = otpReqId
        cy.request({ method: 'POST', url: 'account/sign-in/validate', body: singInData.otpValidation, failOnStatusCode: false })
            .should((response) => {
                expect(response.status).to.eq(400)

            })
    })
    it('OnoArk - Create Profile', () => {
        cy.request({
            method: 'POST',
            url: 'account/profile',
            // form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
            body: {
                fullName: 'DharmaQA',
                gender: 'Male',
                onoId: '210',
                userType: 'Farmer'
            },
            headers: {
                'content-type': 'multipart/form-data',
              }
        })
            .should((response) => {
                expect(response.status).to.eq(200)
            })
    })
    it('OnoArk - Get Profile', () => {
        cy.request(`account/profile/onoId/${onoId}`)
            .should((response) => {
                expect(response.status).to.eq(200)
                expect(response.body.data).length.greaterThan(0)
                expect(response.body.data[0]).to.have.property('prefId')

            })
    })

    it('OnoArk - get Preferences By UserId And Type', () => {
        cy.request('prefs/onoId/180/type/CROP')
            .should((response) => {
                expect(response.status).to.eq(200)
                expect(response.body.data).length.greaterThan(0)
                expect(response.body.data[0]).to.have.property('prefId')

            })
    })
})
