/// <reference types="cypress" />
const commonData = require('../../fixtures/common.json')

context('Common API tests', () => {
    
    it('OnoArk - all language ', () => {
        cy.request('common/langs')
            .should((response) => {
                expect(response.status).to.eq(200)
                expect(response.body.data[0]).to.have.property('langId')
            })
    })
    it('OnoArk - Feedback test', () => {
        cy.request('POST', 'common/feedback', commonData.feedback)
            .should((response) => {
                expect(response.status).to.eq(200)
                expect(response.body).to.be.equal('Feedback has been submitted')

            })
    })
    it('OnoArk - Submit request test', () => {
        cy.request('POST', 'common/submit-request', commonData.subtmitRequest)
            .should((response) => {
                expect(response.status).to.eq(200)
                expect(response.body).to.be.equal('User request has been submitted successfully')

            })
    })

})
